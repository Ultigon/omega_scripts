local m=120239015
local cm=_G["c"..m]
cm.name="深渊龙神 深渊波塞德拉［R］"
function cm.initial_effect(c)
	--Cannot Set Monster
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_MSET)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCondition(cm.setcon1)
	e1:SetTarget(cm.setlimit)
	e1:SetTargetRange(0,1)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetType(EFFECT_TYPE_XMATERIAL+EFFECT_TYPE_FIELD)
	c:RegisterEffect(e2)
	--Cannot Set Spell & Trap
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_XMATERIAL+EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CANNOT_SSET)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetRange(LOCATION_MZONE)
	e3:SetTargetRange(0,1)
	e3:SetCondition(cm.setcon2)
	e3:SetTarget(cm.setlimit)
	c:RegisterEffect(e3)
	--Continuous Effect
	RD.AddContinuousEffect(c,e1,e2,e3)
end
--Cannot Set Monster
function cm.confilter1(c)
	return c:IsType(TYPE_MONSTER)
end
function cm.setcon1(e)
	return not Duel.IsExistingMatchingCard(cm.confilter1,e:GetHandlerPlayer(),LOCATION_GRAVE,0,1,nil)
end
function cm.setlimit(e,c,sump,sumtype,sumpos,targetp,se)
	return c:IsLocation(LOCATION_HAND)
end
--Cannot Set Spell & Trap
function cm.confilter2(c)
	return c:IsFacedown() and c:IsType(TYPE_SPELL+TYPE_TRAP)
end
function cm.setcon2(e)
	return cm.setcon1(e) and RD.MaximumMode(e)
		and Duel.IsExistingMatchingCard(cm.confilter2,e:GetHandlerPlayer(),0,LOCATION_ONFIELD,1,nil)
end
