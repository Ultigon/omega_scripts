local m=120140010
local cm=_G["c"..m]
cm.name="行礼石膏绷带"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DRAW)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCost(cm.cost)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
end
--Activate
function cm.costfilter(c)
	return c:IsPosition(POS_FACEUP_ATTACK) and c:IsLevelAbove(7) and c:IsRace(RACE_WARRIOR)
		and RD.IsCanChangePosition(c)
end
function cm.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.costfilter,tp,LOCATION_MZONE,0,1,nil) end
	RD.SelectAndDoAction(HINTMSG_POSCHANGE,cm.costfilter,tp,LOCATION_MZONE,0,1,1,nil,function(g)
		RD.ChangePosition(g,POS_FACEUP_DEFENSE)
	end)
end
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDraw(tp,1) end
	RD.TargetDraw(tp,1)
end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	RD.Draw()
end