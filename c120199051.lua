local m=120199051
local cm=_G["c"..m]
cm.name="火面浓抽浓厚返之术"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DRAW)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCost(cm.cost)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
end
--Activate
function cm.filter(c)
	return c:IsType(TYPE_NORMAL) and c:IsRace(RACE_PYRO)
end
function cm.exfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_NORMAL) and c:IsRace(RACE_PYRO)
end
function cm.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	local max=math.min(Duel.GetMatchingGroupCount(cm.filter,tp,LOCATION_GRAVE,0,nil),Duel.GetFieldGroupCount(tp,LOCATION_DECK,0))
	if chk==0 then return max>0 and Duel.IsExistingMatchingCard(Card.IsAbleToGraveAsCost,tp,LOCATION_HAND,0,1,e:GetHandler()) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local g=Duel.SelectMatchingCard(tp,Card.IsAbleToGraveAsCost,tp,LOCATION_HAND,0,1,max,nil)
	local ct=Duel.SendtoGrave(g,REASON_COST)
	e:SetLabel(ct)
end
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDraw(tp,1) end
	local ct=e:GetLabel()
	RD.TargetDraw(tp,ct)
end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	if RD.Draw()~=0 and Duel.IsExistingMatchingCard(cm.exfilter,tp,LOCATION_MZONE,0,1,nil) then
		RD.CanDraw(aux.Stringid(m,1),tp,1)
	end
end